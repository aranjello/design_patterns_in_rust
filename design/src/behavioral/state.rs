use std::marker::PhantomData;

pub struct Draft;
pub struct PendingReview;
pub struct Published;

pub struct Document<T> {
    name:String,
    content:String,
    state:PhantomData<T>
}

pub fn new_doc(name:String) -> Document<Draft> {
    Document { name, content: String::new(), state: PhantomData }
}

impl Document<Draft> {
    pub fn change_name(&mut self,name:String) {
        self.name = name;
    }

    pub fn change_content(&mut self, content:String) {
        self.content = content;
    }

    pub fn submit_for_review(self) -> Document<PendingReview> {
        Document { name: self.name, content: self.content, state: PhantomData }
    }
}

impl Document<PendingReview> {
    pub fn change_name(&mut self,name:String) {
        self.name = name;
    }

    pub fn deny(self) -> Document<Draft> {
        Document { name: self.name, content: self.content, state: PhantomData }
    }

    pub fn approve(self) -> Document<Published> {
        Document { name: self.name, content: self.content, state: PhantomData }
    }
}

impl Document<Published> {
    pub fn print_article(&self) {
        const ARTICLE_WIDTH:usize = 50;
        println!("{}","=".repeat(ARTICLE_WIDTH));
        println!("{}",self.name);
        println!("{}","-".repeat(ARTICLE_WIDTH));
        
        let article_formatted = self.content.chars()
        .enumerate()
        .flat_map(|(i, c)| {
            if i != 0 && i % ARTICLE_WIDTH == 0 {
                Some('\n')
            } else {
                None
            }
            .into_iter()
            .chain(std::iter::once(c))
        })
        .collect::<String>();
        println!("{}",article_formatted);
        println!("{}","=".repeat(ARTICLE_WIDTH));
    }
}