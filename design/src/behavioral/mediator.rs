use rand::{Rng, thread_rng};

pub struct Runway{
    index:usize
}

impl Runway{
    pub fn new(index:usize) -> Self{
        Runway { index }
    }
}

pub struct Airplane{
    identifier:usize
}

impl Airplane{

    pub fn new(identifier:usize) -> Self{
        Airplane { identifier }
    }

    fn take_off(&self,runway:&Runway){
        println!("Airplane {:2} taking off on runway {:2}",self.identifier,runway.index);
    }
}

pub struct AirTrafficController{
    planes:Vec<Airplane>,
    runways:Vec<Runway>
}

impl AirTrafficController{
    pub fn new(planes:Vec<Airplane>,runways:Vec<Runway>) -> Self{
        AirTrafficController { planes, runways }
    }

    pub fn send_planes(&mut self) {
        while !self.planes.is_empty(){
            let mut random = thread_rng();
            let plane_index = random.gen_range(0..self.planes.len());
            let runway_index = random.gen_range(0..self.runways.len());
            let plane = self.planes.swap_remove(plane_index);
            let runway = self.runways.swap_remove(runway_index);
            
            plane.take_off(&runway);
        }
    }
}

