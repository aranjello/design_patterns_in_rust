#[derive(Debug)]
pub struct ObjA {
    pub name:String,
    pub value : f32,
}

pub struct Memento {
    pub memento_name:String,
    obj_data_name:String,
    obj_data_value:f32,
}

impl ObjA{
    pub fn new(name:String,value:f32) -> Self{
        ObjA { name , value }
    }

    pub fn create_memento(&self) -> Memento{
        Memento { memento_name: self.name.clone() + "_memento", obj_data_name: self.name.clone(), obj_data_value: self.value }
    }

    pub fn restore(&mut self,mem:&Memento) {
        self.name = mem.obj_data_name.clone();
        self.value = mem.obj_data_value;
    }

}