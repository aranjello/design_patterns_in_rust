#[derive(PartialEq,Debug)]
pub enum ShapeType{
    Square,
    Circle,
    Triangle,
}

#[derive(PartialEq,Debug)]
pub enum Color{
    Red,
    Blue,
    Green
}

#[derive(Debug)]
pub enum ShapeErrors{
    TypeError,
    ColorError,
    SizeError
}

#[derive(Debug)]
pub struct ColorfulShape {
    shape_type:ShapeType,
    shape_color:Color,
    shape_size:f32,
}

fn color_check(shape:&ColorfulShape, color:Color) -> Result<&ColorfulShape,ShapeErrors>{
    if shape.shape_color == color {
        Ok(shape)
    }else{
        Err(ShapeErrors::ColorError)
    }
}

fn shape_check(shape:&ColorfulShape, shape_type:ShapeType) -> Result<&ColorfulShape,ShapeErrors>{
    if shape.shape_type == shape_type {
        Ok(shape)
    }else{
        Err(ShapeErrors::TypeError)
    }
}

fn size_check(shape:&ColorfulShape, size:f32) -> Result<&ColorfulShape,ShapeErrors>{
    if shape.shape_size == size {
        Ok(shape)
    }else{
        Err(ShapeErrors::SizeError)
    }
}

pub fn full_check(shape:&ColorfulShape,color:Color,shape_type:ShapeType,size:f32) -> Result<&ColorfulShape,ShapeErrors> {
    color_check(shape_check(size_check(shape, size)?, shape_type)?, color)
}

impl ColorfulShape {
    pub fn new(shape_type:ShapeType,shape_color:Color,shape_size:f32) -> Self {
        ColorfulShape { shape_type , shape_color , shape_size }
    }
}