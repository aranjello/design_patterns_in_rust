pub struct Subscriber{
    name:String
}

impl Subscriber{
    pub fn new(name:String) -> Self {
        Subscriber { name }
    }

    fn recive_transmission(&self, message: &String) {
        println!("{} recived message {}",self.name,message);
    }
}
#[derive(Default)]
pub struct Publisher {
    subscribers:Vec<Subscriber>
}

impl Publisher {
    pub fn new() -> Self{
        Publisher { subscribers: Vec::new() }
    }

    pub fn add_sub(&mut self, sub:Subscriber) {
        self.subscribers.push(sub);
    }

    pub fn send_message(&self,message:String) {
        for i in &self.subscribers {
            i.recive_transmission(&message);
        }
    }
}