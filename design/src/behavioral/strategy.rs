#[derive(Debug)]
pub struct Thing{
    content:f32
}

pub fn square(num:f32) -> f32 {
    num * num
}

pub fn divide_by_2(num: f32) -> f32 {
    num / 2.0
}

impl Thing{

    pub fn new(val:f32) -> Self {
        Thing { content: val }
    }

    pub fn perform_function(&mut self,func:fn(f32) -> f32) {
        self.content = func(self.content);
    }
}