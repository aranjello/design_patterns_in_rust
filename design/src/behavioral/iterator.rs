
#[derive(Debug,Default)]
pub struct ListOLists {
    list:Vec<Vec<i32>>
}

impl ListOLists{

    pub fn new() -> Self{
        let internal_list = vec![1,2,3];
        ListOLists { list: vec![internal_list.clone(),internal_list] }
    }

    pub fn get_depth_iter(self) -> DepthIterator{
        DepthIterator { index: 0, list: self, list_index: 0 }
    }

    pub fn get_breadth_iter(self) -> BreadthIterator{
        BreadthIterator { index: 0, list: self, list_index: 0 }
    }
}

pub struct DepthIterator{
    index:usize,
    list_index:usize,
    list:ListOLists
}

pub struct BreadthIterator{
    index:usize,
    list_index:usize,
    list:ListOLists
}

impl Iterator for DepthIterator{
    type Item = (usize,usize,i32);

    fn next(&mut self) -> Option<Self::Item> {
        loop{
            match self.list.list.get(self.list_index) {
                Some(internal_list) => {
                    match internal_list.get(self.index) {
                        Some(value) => {
                            let ret_val = Some((self.list_index,self.index,*value));
                            self.index += 1;
                            return ret_val;
                        },
                        None => {
                            self.list_index += 1;
                            self.index = 0;
                        },
                    }
                },
                None => return None,
            }
        }
    }
}

impl Iterator for BreadthIterator {
    type Item = (usize,usize,i32);

    fn next(&mut self) -> Option<Self::Item> {
        loop{
            match self.list.list.get(self.list_index) {
                Some(internal_list) => {
                    match internal_list.get(self.index) {
                        Some(value) => {
                            let ret_val = Some((self.list_index,self.index,*value));
                            self.list_index += 1;
                            return ret_val;
                        },
                        None => return None,
                    }
                },
                None => {
                    self.index += 1;
                    self.list_index = 0;
                },
            }
        }
    }
}