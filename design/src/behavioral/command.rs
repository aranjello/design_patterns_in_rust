pub struct Command {
    name:String,
    data:f32
}

pub struct Reciever;

impl Reciever{
    pub fn only_takes_commands(&self,command:Command){
        println!("Recieved command {} with data {}",command.name,command.data);
    }
}

impl Command{
    pub fn create_command(name:String, data:f32) -> Self {
        Command { name, data }
    }
}

pub struct Sender1{
    name:String,
    data:f32
}

impl Sender1{

    pub fn new(name:String, data:f32) -> Self{
        Sender1 { name, data }
    }

    pub fn generate_command(&self) -> Command{
        Command { name: self.name.clone(), data: self.data }
    }
}