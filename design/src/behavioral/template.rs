pub struct ThingDoer {
    sub_doer:Box<dyn DoesThings>
}

pub trait DoesThings {
    fn do_a(&self);
    fn do_b(&self);
    fn do_c(&self);
}

pub struct SubA;

impl DoesThings for SubA {
    fn do_a(&self) {
        println!("Sub A does A");
    }

    fn do_b(&self) {
        println!("Sub A does B");
    }

    fn do_c(&self) {
        println!("Sub A does C");
    }
}

impl ThingDoer {
    pub fn new(sub_doer:Box<dyn DoesThings>) -> Self {
        ThingDoer { sub_doer }
    }

    pub fn perform_actions(&self) {
        println!("ThingDoer doing A!");
        self.sub_doer.do_a();
        println!("ThingDoer doing B!");
        self.sub_doer.do_b();
        println!("ThingDoer doing C!");
        self.sub_doer.do_c();
    }
}