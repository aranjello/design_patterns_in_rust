pub struct ThingA {
    name:String,
    value:f32,
}

impl ThingA {
    pub fn new(name:String, value:f32) -> Self {
        ThingA { name, value }
    }
}

pub struct ThingB {
    name:String,
    value:usize
}

impl ThingB {
    pub fn new(name:String, value:usize) -> Self {
        ThingB { name, value }
    }
}

pub trait AcceptVisitor {
    fn accept_visitor(&self,visitor:&Visitor);
}

impl AcceptVisitor for ThingA {
    fn accept_visitor(&self,visitor:&Visitor) {
        visitor.visit_a(self);
    }
}

impl AcceptVisitor for ThingB {
    fn accept_visitor(&self,visitor:&Visitor) {
        visitor.visit_b(self);
    }
}

pub struct Visitor;

impl Visitor {
    fn visit_a(&self,thing:&ThingA) {
        println!("Visited thing A with name {} and value {}",thing.name,thing.value);
    }

    fn visit_b(&self,thing:&ThingB) {
        println!("Visited thing B with name {} and value {}",thing.name,thing.value);
    }
}