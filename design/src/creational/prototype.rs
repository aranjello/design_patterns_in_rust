#[derive(Clone,Debug)]
pub struct Circle {
    pub radius: u32,
}

#[derive(Clone,Debug)]
pub struct Square {
    pub diagonal: u32,
}