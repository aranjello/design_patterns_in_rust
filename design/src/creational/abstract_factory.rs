//define the interfaces for the concrete objects we will end up using
pub trait Button {
    fn click(&self) -> String;
}

pub trait Slider {
    fn slide(&mut self, val:u8);
}

struct FancyButton {
    name : String,
    weight: i32,
}

impl Button for FancyButton {
    fn click(&self) -> String{
        format!("my name is {} and my weight is {} and I am \"VERY\" fancy",self.name,self.weight)
    }
}

struct FancySlider {
    name:String,
    position:u8
}

impl Slider for FancySlider {
    fn slide(&mut self, val:u8) {
        let old_slide_val = self.position;
        self.position = val;
        println!("Fancy Slider {} moved from {} to {} in a fancy way!",self.name,old_slide_val,self.position);
    }
}

struct HeavyButton {
    name   : String,
    weight : i32,
}

impl Button for HeavyButton {
    fn click(&self) -> String{
        format!("my name is {} and my weight is {} and I am \"VERY\" heavy",self.name,self.weight)
    }
}

struct HeavySlider {
    name: String,
    position: u8
}

impl Slider for HeavySlider {
    fn slide(&mut self, val:u8) {
        let old_slide_val = self.position;
        self.position = val;
        println!("Heavy Slider {} moved from {} to {} with difficulty!",self.name,old_slide_val,self.position);
    }
}

trait ItemMaker {
    fn make_button(&self) -> Box<dyn Button>;

    fn make_slider(&self) -> Box<dyn Slider>;
}

struct FancyItemMaker;

impl ItemMaker for FancyItemMaker{
    fn make_button(&self) -> Box<dyn Button> {
        Box::new(FancyButton{name: "fancy".to_string(), weight: 10})
    }

    fn make_slider(&self) -> Box<dyn Slider> {
        Box::new(FancySlider{ name:"fancy".to_string(), position:0 })
    }
}

struct HeavyItemMaker;

impl ItemMaker for HeavyItemMaker {
    fn make_button(&self) -> Box<dyn Button> {
        Box::new(HeavyButton{ name: "heavy".to_string(), weight: 100 })
    }

    fn make_slider(&self) -> Box<dyn Slider> {
        Box::new(HeavySlider{ name: "heavy".to_string(), position: 0 })
    }
}

pub struct ObjFactory{
    factory: Box<dyn ItemMaker>,
}

pub enum ItemType{
    Fancy,
    Heavy,
}

impl ObjFactory{
    pub fn new(factory: ItemType) -> Self{
        match factory {
            ItemType::Fancy => ObjFactory { factory: Box::new(FancyItemMaker) },
            ItemType::Heavy => ObjFactory { factory: Box::new(HeavyItemMaker) },
        }
        
    }

    pub fn get_button(&self) -> Box<dyn Button>{
        self.factory.make_button()
    }

    pub fn get_slider(&self) -> Box<dyn Slider> {
        self.factory.make_slider()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn get_fancy_buttons() {
        let item_factory = ObjFactory::new(ItemType::Fancy);
        let button         = item_factory.get_button();
        assert_eq!(button.click(),"my name is fancy and my weight is 10 and I am \"VERY\" fancy");
        let mut slider = item_factory.get_slider();
        slider.slide(25);
    }
}