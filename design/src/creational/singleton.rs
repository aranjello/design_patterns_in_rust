use std::sync::Mutex;

static SINGLETON: Mutex<Data> = Mutex::new(Data{ info: None});

#[derive(Debug,Clone)]
pub struct Data{
    pub info:Option<usize>
}

pub fn set_data(num:usize) {
    SINGLETON.lock().unwrap().info = Some(num);
}

pub fn get_data() -> Data{
    let mut data = SINGLETON.lock().unwrap();
    if data.info.is_none() {
        data.info = Some(0);
    }
    data.clone()
}