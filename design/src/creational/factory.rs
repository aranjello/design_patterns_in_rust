pub trait CanDeliver{
    fn pick_up(&self, load:usize) -> bool;
    fn drop_off(&self) -> bool;
}

struct Truck{
    bed_size : usize,
    has_load : bool,
}

impl CanDeliver for Truck{
    fn pick_up(&self, load:usize) -> bool {
        println!("Truck picking up");
        load <= self.bed_size && !self.has_load
    }

    fn drop_off(&self) -> bool {
        println!("Truck dropping off");
        self.has_load
    }
}

struct Boat{
    hold_size : usize,
    has_load : bool,
}

impl CanDeliver for Boat{
    fn pick_up(&self, load:usize) -> bool {
        println!("Boat picking up");
        load <= self.hold_size && !self.has_load
    }

    fn drop_off(&self) -> bool {
        println!("Boat dropping off");
        self.has_load
    }
}

pub enum VehicleType{
    Truck,
    Boat
}

pub struct DeliveryFactory;

impl DeliveryFactory{
    pub fn get_delivery_vehicle(transport_type: VehicleType) -> Box<dyn CanDeliver>{
        match transport_type {
            VehicleType::Truck => Box::new(Truck{bed_size: 10, has_load: false}),
            VehicleType::Boat => Box::new(Boat{hold_size: 10, has_load: false}),
        }
    }
}