use std::marker::PhantomData;

#[derive(Debug)]
pub enum SnowmanHat {
    TopHat,
    Scarf,
    StrawHat,
    BowlerHat,
    CowboyHat,
}

#[derive(Debug)]
pub enum SnowmanGlasses{
    Sunglasses,
    Glasses,
    Pipe,
}

#[derive(Debug)]
pub enum SnowmanJacket {
    Coat,
    ShirtButtons,
}

pub enum SnowmanDecoration{
    Hat(SnowmanHat),
    Glasses(SnowmanGlasses),
    Jacket(SnowmanJacket),
}

pub struct NotSpecified;
pub struct Named;
pub struct HasJacket;
pub struct HasHat;
pub struct HasGlasses;

pub struct Snowman{
    name        : Option<String>,
    layers      : usize,
    decorations : Vec<SnowmanDecoration>,
    
}

impl Snowman{
    pub fn describe(&self){
        println!("Hi my name is {} and I am a snowman!",self.name.as_ref().expect("snowman is named"));
        println!("I am {} layers tall!",self.layers);
        for decoration in self.decorations.iter(){
            match decoration {
                SnowmanDecoration::Hat(hat) => println!("I am wearing a {:?}",hat),
                SnowmanDecoration::Glasses(glasses) => println!("I am wearing a {:?}",glasses),
                SnowmanDecoration::Jacket(jacket) => println!("I am wearing a {:?}",jacket),
            }
            
        }
    }
}

pub struct SnowmanBuilder<N,H,G,J>{
    snowman  : Snowman,
    named    : PhantomData<N>,
    hatted   : PhantomData<H>,
    glassed  : PhantomData<G>,
    jacketed : PhantomData<J>
}

impl<N,H,G,J> Default for SnowmanBuilder<N,H,G,J>{
    fn default() -> Self {
        SnowmanBuilder { 
            snowman : Snowman { name:None, layers: 0, decorations: Vec::new() },
            named   : PhantomData,
            hatted  : PhantomData,
            glassed : PhantomData,
            jacketed: PhantomData,   
        }
    }
}

pub fn get_snowman_builder() -> SnowmanBuilder<NotSpecified,NotSpecified,NotSpecified,NotSpecified>{
    SnowmanBuilder { ..Default::default() }
}
impl<N,H,G,J> SnowmanBuilder<N,H,G,J>{

    pub fn add_layer(mut self) -> Self{
        self.snowman.layers += 1;
        SnowmanBuilder { snowman: self.snowman, ..Default::default() }
    }
    
}

impl <H,G,J> SnowmanBuilder<NotSpecified,H,G,J> {
    pub fn name(mut self, name: impl Into<String>) -> SnowmanBuilder<Named,H,G,J>{
        self.snowman.name = Some(name.into());
        
        SnowmanBuilder { snowman: self.snowman, ..Default::default() }
    }
}

impl<N,G,J> SnowmanBuilder<N, NotSpecified, G, J>{
    pub fn add_hat(mut self, hat: SnowmanHat) -> SnowmanBuilder<N, HasHat, G, J>{
        self.snowman.decorations.push(SnowmanDecoration::Hat(hat));
        SnowmanBuilder { snowman: self.snowman, ..Default::default() }
    }
}

impl<N,H,J> SnowmanBuilder<N, H, NotSpecified, J>{
    pub fn add_glasses(mut self, glasses: SnowmanGlasses) -> SnowmanBuilder<N, H, HasGlasses, J>{
        self.snowman.decorations.push(SnowmanDecoration::Glasses(glasses));
        SnowmanBuilder { snowman: self.snowman, ..Default::default() }
    }
}

impl<N,H,G> SnowmanBuilder<N, H, G, NotSpecified>{
    pub fn add_jacket(mut self, jacket: SnowmanJacket) -> SnowmanBuilder<N, H, G, HasJacket>{
        self.snowman.decorations.push(SnowmanDecoration::Jacket(jacket));
        SnowmanBuilder { snowman: self.snowman, ..Default::default() }
    }
}

impl<H,G,J> SnowmanBuilder<Named,H,G,J>{
    pub fn get_snowman(self) -> Snowman{
        self.snowman
    }
}