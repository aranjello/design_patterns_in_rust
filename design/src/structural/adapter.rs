use std::ops::{Deref, DerefMut, AddAssign};

#[derive(Debug,Clone)]
pub struct Meters(f32);
#[derive(Debug,Clone)]
pub struct Kilometers(f32);

#[derive(Debug,Clone)]
pub struct Feet(f32);

impl Meters{
    pub fn new(value:f32) -> Self{
        Meters(value)
    }
}

impl Kilometers{
    pub fn new(value:f32) -> Self{
        Kilometers(value)
    }
}

impl From<Feet> for Meters {
    fn from(value: Feet) -> Self {
        Meters(value.0 * 0.3048)
    }
}

impl From<Meters> for Feet {
    fn from(value: Meters) -> Self {
        Feet(value.0 * 3.28084)
    }
}

impl From<Feet> for Kilometers{
    fn from(value: Feet) -> Self {
        let m: Meters = value.into();
        m.into()
    }
}

impl From<Kilometers> for Meters {
    fn from(value: Kilometers) -> Self {
        Meters(value.0 * 1000.0)
    }
}

impl From<Meters> for Kilometers{
    fn from(value: Meters) -> Self {
        Kilometers(value.0 / 1000.0)
    }
}

impl From<Kilometers> for Feet {
    fn from(value: Kilometers) -> Self {
        value.into()
    }
}

impl Deref for Meters{
    type Target = f32;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Deref for Feet{
    type Target = f32;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Meters {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl AddAssign<Feet> for Meters{
    fn add_assign(&mut self, rhs: Feet) {
        self.0 += rhs.0
    }
}


// impl From<ImperialMeasurment> for MetricMeasurment{
//     fn from(value: ImperialMeasurment) -> Self {
//         match value {
//             ImperialMeasurment::Feet(ft) => MetricMeasurment::Meters(ft * 0.3048),
//             ImperialMeasurment::Pounds(lbs) => MetricMeasurment::Kilograms(lbs * 0.453592),
//         }
//     }
// }

// impl From<MetricMeasurment> for ImperialMeasurment{
//     fn from(value: MetricMeasurment) -> Self {
//         match value {
//             MetricMeasurment::Meters(m) => ImperialMeasurment::Feet(m * 3.28084),
//             MetricMeasurment::Kilograms(kg) => ImperialMeasurment::Pounds(kg * 2.20462),
//         }
//     }
// }