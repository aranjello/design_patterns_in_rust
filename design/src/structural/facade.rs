pub struct Converter;

fn conversion(float1:f32,float2:f32) -> bool{
    float1 < float2
}

fn is_weekday() -> bool {
    //complicated stuff to find out if it is a weekday today
    true
}

impl Converter{
    pub fn is_str_less_than_str(f1:&str,f2:&str) -> bool{
        conversion(f1.parse().unwrap(), f2.parse().unwrap()) && is_weekday()
    }
}