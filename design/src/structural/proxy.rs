use std::{time::Duration, thread::sleep};

#[derive(Default)]
struct SlowDelicateThing;

impl SlowDelicateThing{
    pub fn process_data(&self,text:&str) -> f32{
        sleep(Duration::from_secs(1));
        text.parse().unwrap()
    }
}

#[derive(Default)]
pub struct SlowDeviceProxy{
    text:Vec<String>,
    delicate_thing:SlowDelicateThing
}

impl SlowDeviceProxy {
    pub fn new() -> Self{
        SlowDeviceProxy { text: Vec::new(), delicate_thing: SlowDelicateThing }
    }

    pub fn add_work(&mut self,text:String){
        self.text.push(text);
    }

    pub fn pass_data_to_proxy(&mut self) -> f32 {
        self.delicate_thing.process_data(&self.text.pop().unwrap())
    }
}
