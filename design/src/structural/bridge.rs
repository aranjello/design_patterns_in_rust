pub trait Controllabe {
    fn move_forward(&mut self);
    fn turn_left(&mut self);
    fn turn_right(&mut self);
    fn check_position(&self);

}

#[derive(Default)]
pub struct Car{
    x:f32,
    y:f32,
    dir:f32
}

impl Car {
    pub fn new() -> Self{
        Car { x: 0.0, y: 0.0, dir: 0.0 }
    }
}

impl Controllabe for Car {
    fn move_forward(&mut self) {
        self.x += self.dir.sin();
        self.y += self.dir.cos();
    }

    fn turn_left(&mut self) {
        self.dir += 1.0;
    }

    fn turn_right(&mut self) {
        self.dir -= 1.0;
    }

    fn check_position(&self) {
        println!("car is at {} {} pointed {}",self.x,self.y,self.dir);
    }

}

pub struct KinematicController{
    controlled:Box<dyn Controllabe>
}

impl KinematicController {
    pub fn create_link(object:Box<dyn Controllabe>) -> Self{
        KinematicController { controlled: object }
    }

    pub fn twist_left(&mut self){
        self.controlled.turn_left();
    }

    pub fn twist_right(&mut self){
        self.controlled.turn_right();
    }

    pub fn tip_forward(&mut self){
        self.controlled.move_forward();
    }

    pub fn feel_for_controllable(&self){
        self.controlled.check_position();
    }

}

pub struct ThoughtController{
    controlled:Box<dyn Controllabe>
}

impl ThoughtController {
    pub fn create_link(object:Box<dyn Controllabe>) -> Self{
        ThoughtController { controlled: object }
    }

    pub fn think_left(&mut self){
        self.controlled.turn_left();
    }

    pub fn think_right(&mut self){
        self.controlled.turn_right();
    }

    pub fn think_forward(&mut self){
        self.controlled.move_forward();
    }

    pub fn telepathic_link(&self){
        self.controlled.check_position();
    }
}
