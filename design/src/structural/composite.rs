struct FastTire(usize);
struct OffroadTire(usize);

trait Tire {
    fn spin(&self,value:f32);
}

impl Default for Box<dyn Tire>{
    fn default() -> Self {
        Box::new(FastTire(0))
    }
}

impl Tire for FastTire{
    fn spin(&self,value:f32){
        println!("Fast tire {} is spinning {}",self.0,value);
    }
}

impl Tire for OffroadTire{
    fn spin(&self,value:f32){
        println!("Offroad tire {} is spinning {}",self.0,value);
    }
}

#[derive(Default)]
struct Transmission {
    index: usize,
    tires: [Box<dyn Tire>;2]
}

impl Transmission{
    fn new(index:usize) -> Self{
        if index == 0{
            Transmission { index, tires:[Box::new(OffroadTire(index*2+1)),Box::new(OffroadTire(index*2+2))] }
        }else{
            Transmission { index, tires:[Box::new(FastTire(index*2+1)),Box::new(FastTire(index*2+2))] }
        }
    }

    fn convert_power(&self, hp:f32){
        let spin = hp/self.tires.len() as f32;
        println!("Transmission {} is converting {} hp to {} spin for each tire",self.index,hp,spin);
        for i in &self.tires{
            i.spin(spin);
        }
    }
}

#[derive(Default)]
pub struct Engine {
    transmissions:[Transmission;2]
}


impl Engine {
    pub fn new() -> Self{
        Engine { transmissions: [Transmission::new(0),Transmission::new(1)] }
    }

    pub fn create_power(&self, hp:f32){
        for i in &self.transmissions{
            i.convert_power(hp)
        }
    }
}