pub struct HeavyData {
    big_num:[i64;10000]
}

impl Default for HeavyData{
    fn default() -> Self {
        Self { big_num: [0;10000] }
    }
}

impl HeavyData{
    pub fn new() -> Self{
        let mut num_arr = [0;10000];
        for (index,i) in num_arr.iter_mut().enumerate() {
            *i = index as i64;
        }
        HeavyData { big_num: num_arr }
    }
}

pub struct HeavierObject {
    num_ref: HeavyData,
    name:String
}

impl HeavierObject {
    pub fn new(heavy_data:HeavyData,name:String) -> Self{
        HeavierObject { num_ref: heavy_data , name}
    }

    pub fn say_hello(&self) {
        println!("name is {} data is {}",self.name,self.num_ref.big_num[100]);
    }
}

pub struct LightObject<'a> {
    num_ref:&'a HeavyData,
    name:String
}

impl<'a> LightObject<'a> {
    pub fn say_hello(&self) {
        println!("name is {} data is {}",self.name,self.num_ref.big_num[100]);
    }
}

pub struct ObjectFactory{
    data:HeavyData
}

impl ObjectFactory {
    pub fn new(data:HeavyData) -> Self{
        ObjectFactory { data }
    }

    pub fn create_object(&self,name:String) -> LightObject{
        LightObject { num_ref: &self.data, name }
    }
}

