use design_patterns_in_rust::prototype;

fn main(){
    let small_proto_circle = prototype::Circle{radius: 45};
    let large_proto_circle = prototype::Circle{radius: 500};
    let new_circle = small_proto_circle.clone();
    let other_circle = large_proto_circle.clone();
    println!("got a small circle {new_circle:?} and a large circle {other_circle:?}")
}