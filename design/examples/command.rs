use design_patterns_in_rust::command;

fn main(){
    let sender = command::Sender1::new("Jim".to_string(), 100.0);
    let com = sender.generate_command();
    let recv = command::Reciever;
    recv.only_takes_commands(com);
}