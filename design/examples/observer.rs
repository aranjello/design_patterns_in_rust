use design_patterns_in_rust::observer;

fn main() {
    let mut publisher = observer::Publisher::new();
    for i in 0..10 {
        publisher.add_sub(observer::Subscriber::new(i.to_string()));
    }
    publisher.send_message("Hello everyone".to_string());
}