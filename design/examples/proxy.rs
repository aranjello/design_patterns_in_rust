use design_patterns_in_rust::proxy;

fn main(){
    let mut x = proxy::SlowDeviceProxy::new();
    x.add_work("1".to_string());
    x.add_work("1.5".to_string());
    x.add_work("2.6".to_string());
    println!("Processing");
    println!("processed data {}",x.pass_data_to_proxy());
    println!("processed data {}",x.pass_data_to_proxy());
    println!("processed data {}",x.pass_data_to_proxy());
}