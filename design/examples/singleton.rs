use design_patterns_in_rust::singleton;

fn main() {
    singleton::get_data();
    println!("single data is {:?}",singleton::get_data());
    change_singleton();
    println!("single data is {:?}",singleton::get_data());
}

fn change_singleton(){
    singleton::set_data(100);
}