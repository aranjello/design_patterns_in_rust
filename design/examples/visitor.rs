use design_patterns_in_rust::visitor::{self, AcceptVisitor};

fn main() {
    let thing_a = visitor::ThingA::new("Jim".to_string(),100.0);
    let thing_b = visitor::ThingB::new("Jam".to_string(),50);
    let vis = visitor::Visitor;
    thing_a.accept_visitor(&vis);
    thing_b.accept_visitor(&vis);
}