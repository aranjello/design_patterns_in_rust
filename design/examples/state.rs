use design_patterns_in_rust::state;

fn main(){
    let mut new_article = state::new_doc("Bing bong goes bad".to_string());
    new_article.change_content("Bing bong has bitten the bad burger and begun badgering babies and bums in big blunder for his new film bing bong brings it back".to_string());
    let pending_article = new_article.submit_for_review();
    let final_article = pending_article.approve();
    final_article.print_article();
}