use design_patterns_in_rust::template;

fn main() {
    let sub = template::SubA;
    let doer = template::ThingDoer::new(Box::new(sub));
    doer.perform_actions();
}