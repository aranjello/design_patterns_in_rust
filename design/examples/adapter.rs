use design_patterns_in_rust::adapter::{self, Kilometers};

fn main(){
    let new_meters = adapter::Meters::new(45.0);
    let new_feet:adapter::Feet = new_meters.clone().into();
    let newer_meters:adapter::Meters = new_feet.clone().into();
    do_measure(&new_meters.into());
    do_measure(&new_feet.into());
    do_measure(&newer_meters.into());
}

fn do_measure(value: &Kilometers){
    println!("num as meters {:?}",*value)
}