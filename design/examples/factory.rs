use design_patterns_in_rust::factory;
fn main(){
    let truck = factory::DeliveryFactory::get_delivery_vehicle(factory::VehicleType::Truck);
    let boat  = factory::DeliveryFactory::get_delivery_vehicle(factory::VehicleType::Boat);
    
    truck.pick_up(25);
    boat.pick_up(50);
    truck.drop_off();
    boat.drop_off();
}