use design_patterns_in_rust::memento;

fn main(){
    let mut obj = memento::ObjA::new("John".to_string(), 100.0);
    let mem = obj.create_memento();
    println!("obj is {:?}",obj);
    obj.name = "Stebem".to_string();
    println!("obj is {:?}",obj);
    let mem2 = obj.create_memento();
    obj.value = 50.0;
    println!("obj is {:?}",obj);
    let mem3 = obj.create_memento();
    obj.restore(&mem);
    println!("restoring {} obj is {:?}",mem.memento_name,obj);
    obj.restore(&mem3);
    println!("restoring {} obj is {:?}",mem3.memento_name,obj);
    obj.restore(&mem2);
    println!("restoring {} obj is {:?}",mem2.memento_name,obj);
}