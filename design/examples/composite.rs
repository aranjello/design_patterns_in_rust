use design_patterns_in_rust::composite;

fn main(){
    let car = composite::Engine::new();
    car.create_power(100.0);
}