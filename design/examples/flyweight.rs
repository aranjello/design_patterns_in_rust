use std::{time::{Duration, Instant}, thread};

use design_patterns_in_rust::flyweight;

fn main(){
    
    let mut heavy_vec = Vec::new();

    let now = Instant::now();
    for i in 0..10000{
        heavy_vec.push(flyweight::HeavierObject::new(flyweight::HeavyData::new(),format!("Index {}",i).to_string()));
    }

    println!("Created heavy objects in {}",now.elapsed().as_millis());
    thread::sleep(Duration::from_secs(5));
    drop(heavy_vec);

    let data = flyweight::HeavyData::new();
    let mut obj_vec = Vec::new();
    let obj_factory = flyweight::ObjectFactory::new(data);

    let now = Instant::now();
    for i in 0..10000000 {
        obj_vec.push(obj_factory.create_object(format!("Index {}",i).to_string()));
    }

    obj_vec[100].say_hello();
    println!("Created flyweights in {}",now.elapsed().as_millis());
    thread::sleep(Duration::from_secs(5));
}