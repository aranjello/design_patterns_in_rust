use design_patterns_in_rust::chain_of_responsibility;
use chain_of_responsibility::{Color,ShapeType};
fn main(){
    let shape_type = ShapeType::Circle;
    let shape_color = Color::Red;
    let shape_size = 10.5;
    let shape = chain_of_responsibility::ColorfulShape::new(shape_type, shape_color, shape_size);
    let result = chain_of_responsibility::full_check(&shape, Color::Blue, ShapeType::Square, 1.0);
    match_result(result);
    let result = chain_of_responsibility::full_check(&shape, Color::Blue, ShapeType::Square, 10.5);
    match_result(result);
    let result = chain_of_responsibility::full_check(&shape, Color::Blue, ShapeType::Circle, 10.5);
    match_result(result);
    let result = chain_of_responsibility::full_check(&shape, Color::Red, ShapeType::Circle, 10.5);
    match_result(result);
}

fn match_result(match_on:Result<&chain_of_responsibility::ColorfulShape, chain_of_responsibility::ShapeErrors>){
    match match_on {
        Ok(s) => println!("Got Shape {s:?}"),
        Err(e) => println!("Error {e:?}"),
    }
}