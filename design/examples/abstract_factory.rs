use design_patterns_in_rust::abstract_factory;

fn main(){
    let item_factory = abstract_factory::ObjFactory::new(abstract_factory::ItemType::Fancy);
    let button         = item_factory.get_button();
    println!("{}",button.click());
    let mut slider = item_factory.get_slider();
    slider.slide(25);

    let item_factory = abstract_factory::ObjFactory::new(abstract_factory::ItemType::Heavy);
    let button         = item_factory.get_button();
    println!("{}",button.click());
    let mut slider = item_factory.get_slider();
    slider.slide(25);
}

