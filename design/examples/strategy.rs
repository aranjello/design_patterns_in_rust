use design_patterns_in_rust::strategy;

fn main() {
    let mut thing = strategy::Thing::new(100.0);
    thing.perform_function(strategy::square);
    println!("{:?}",thing);
    thing.perform_function(strategy::divide_by_2);
    println!("{:?}",thing);
    thing.perform_function(|x| x.sqrt());
    println!("{:?}",thing);

}