use design_patterns_in_rust::mediator;

fn main(){
    let len_stuff = 100;
    let mut planes = Vec::new();
    for i in 0..len_stuff {
        planes.push(mediator::Airplane::new(i))
    }

    let mut runways = Vec::new();
    for i in 0..len_stuff {
        runways.push(mediator::Runway::new(i))
    }

    let mut controller = mediator::AirTrafficController::new(planes, runways);
    controller.send_planes();
}