use design_patterns_in_rust::bridge;

fn main(){
    let car = bridge::Car::new();
    let mut thinker = bridge::ThoughtController::create_link(Box::new(car));
    let car2 = bridge::Car::new();
    let mut mover = bridge::KinematicController::create_link(Box::new(car2));

    thinker.think_forward();
    thinker.telepathic_link();
    thinker.think_left();
    thinker.think_forward();
    thinker.telepathic_link();
    
    mover.tip_forward();
}