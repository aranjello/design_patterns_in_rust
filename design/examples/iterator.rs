use design_patterns_in_rust::iterator;

fn main(){
    let list = iterator::ListOLists::new();
    println!("List is {list:?}");
    let depth_iter = list.get_depth_iter();
    for (outer_list,inner_list,i) in depth_iter{
        println!("depth iter for outer {outer_list} inner {inner_list} value {i}");
    }

    let list = iterator::ListOLists::new();
    let breadth_iter = list.get_breadth_iter();
    for (outer_list,inner_list,i) in breadth_iter{
        println!("breadth iter for outer {outer_list} inner {inner_list} value {i}");
    }
}