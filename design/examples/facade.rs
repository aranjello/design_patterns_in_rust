use design_patterns_in_rust::facade;

fn main(){
    let x = "1";
    let y = ".5";
    println!("x is less than y is {}",facade::Converter::is_str_less_than_str(x, y));
}