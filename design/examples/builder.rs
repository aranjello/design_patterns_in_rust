use design_patterns_in_rust::builder::{SnowmanGlasses,SnowmanJacket,SnowmanHat,get_snowman_builder};
fn main(){
    let sb = get_snowman_builder();

    let snowman = sb.add_layer()
                    .add_glasses(SnowmanGlasses::Glasses)
                    .name("Bob")
                    .add_layer()
                    .add_jacket(SnowmanJacket::Coat)
                    .add_hat(SnowmanHat::CowboyHat)
                    .add_layer()
                    .get_snowman();
                
    snowman.describe();
}