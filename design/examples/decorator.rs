use design_patterns_in_rust::decorator;
use my_macro::HelloMacro;

fn main(){
    decorator::TestMacro::hello_macro();
}